DEBEMAIL="clint@debian.org"

export DEBEMAIL

_JAVA_AWT_WM_NONREPARENTING=1
export _JAVA_AWT_WM_NONREPARENTING

GDK_SCALE=2
export GDK_SCALE
QT_AUTO_SCREEN_SCALE_FACTOR=1
export QT_AUTO_SCREEN_SCALE_FACTOR
GDK_DPI_SCALE=0.5
export GDK_DPI_SCALE
