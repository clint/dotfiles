{-# LANGUAGE OverloadedStrings #-}

module Main where

import Data.Maybe (fromMaybe)

import Termonad
  ( CursorBlinkMode(CursorBlinkModeOff)
  , Option(Set)
  , ShowScrollbar(ShowScrollbarNever)
  , TMConfig
  , confirmExit
  , cursorBlinkMode
  , defaultConfigOptions
  , defaultTMConfig
  , options
  , showMenu
  , showScrollbar
  , start
  , defaultFontConfig
  , fontConfig
  , FontConfig(..)
  , FontSize(FontSizePoints)
  )
import Termonad.Config.Colour
  ( AlphaColour
  , ColourConfig
  , Palette(ExtendedPalette)
  , addColourExtension
  , createColour
  , createColourExtension
  , defaultColourConfig
  , defaultStandardColours
  , defaultLightColours
  , backgroundColour
  , foregroundColour
  , palette
  , List8
  , mkList8
  )

myTMConfig :: TMConfig
myTMConfig =
  defaultTMConfig
    { options =
        defaultConfigOptions
          { showScrollbar = ShowScrollbarNever
          , confirmExit = False
          , showMenu = False
          , cursorBlinkMode = CursorBlinkModeOff
          , fontConfig = fontConf
          }
    }

solarizedDark :: ColourConfig (AlphaColour Double)
solarizedDark =
  defaultColourConfig
    -- Set the default foreground colour of text of the terminal.
    { foregroundColour = Set (createColour 131 148 150) -- base0
    , backgroundColour = Set (createColour   0  43  54) -- base03
    -- Set the extended palette that has 2 Vecs of 8 Solarized palette colours
    , palette = ExtendedPalette solarizedDark1 solarizedDark2
    }
  where
    solarizedDark1 :: List8 (AlphaColour Double)
    solarizedDark1 = fromMaybe defaultStandardColours $ mkList8
      [ createColour   7  54  66 -- base02
      , createColour 220  50  47 -- red
      , createColour 133 153   0 -- green
      , createColour 181 137   0 -- yellow
      , createColour  38 139 210 -- blue
      , createColour 211  54 130 -- magenta
      , createColour  42 161 152 -- cyan
      , createColour 238 232 213 -- base2
      ]

    solarizedDark2 :: List8 (AlphaColour Double)
    solarizedDark2 = fromMaybe defaultStandardColours $ mkList8
      [ createColour   0  43  54 -- base03
      , createColour 203  75  22 -- orange
      , createColour  88 110 117 -- base01
      , createColour 101 123 131 -- base00
      , createColour 131 148 150 -- base0
      , createColour 108 113 196 -- violet
      , createColour 147 161 161 -- base1
      , createColour 253 246 227 -- base3
      ]

-- This is our Solarized light 'ColourConfig'.  It holds all of our light-related settings.
solarizedLight :: ColourConfig (AlphaColour Double)
solarizedLight =
  defaultColourConfig
    -- Set the default foreground colour of text of the terminal.
    { foregroundColour = Set (createColour 101 123 131) -- base00
    , backgroundColour = Set (createColour 253 246 227) -- base3
    -- Set the extended palette that has 2 Vecs of 8 Solarized palette colours
    , palette = ExtendedPalette solarizedLight1 solarizedLight2
    }
  where
    solarizedLight1 :: List8 (AlphaColour Double)
    solarizedLight1 = fromMaybe defaultLightColours $ mkList8
      [ createColour   7  54  66 -- base02
      , createColour 220  50  47 -- red
      , createColour 133 153   0 -- green
      , createColour 181 137   0 -- yellow
      , createColour  38 139 210 -- blue
      , createColour 211  54 130 -- magenta
      , createColour  42 161 152 -- cyan
      , createColour 238 232 213 -- base2
      ]

    solarizedLight2 :: List8 (AlphaColour Double)
    solarizedLight2 = fromMaybe defaultLightColours $ mkList8
      [ createColour   0  43  54 -- base03
      , createColour 203  75  22 -- orange
      , createColour  88 110 117 -- base01
      , createColour 101 123 131 -- base00
      , createColour 131 148 150 -- base0
      , createColour 108 113 196 -- violet
      , createColour 147 161 161 -- base1
      , createColour 253 246 227 -- base3
      ]

-- This defines the font for the terminal.
fontConf :: FontConfig
fontConf =
  defaultFontConfig
    { fontFamily = "Hack"
    , fontSize = FontSizePoints 14
    }


main :: IO ()
main = do
  myColourExt <- createColourExtension solarizedLight

  let newTMConfig = addColourExtension myTMConfig myColourExt
  start newTMConfig

{-
s_base03 = sRGB24read "#002b36"
s_base02 = sRGB24read "#073642"
s_base01 = sRGB24read "#586e75"
s_base00 = sRGB24read "#657b83"
s_base0  = sRGB24read "#839496"
s_base1  = sRGB24read "#93a1a1"
s_base2  = sRGB24read "#eee8d5"
s_base3  = sRGB24read "#fdf6e3"

bgcolor = s_base3
fgcolor = s_base00
cursColor = s_base01
-}

{-
URxvt*fadeColor:             S_base3
URxvt*cursorColor:           S_base01
URxvt*pointerColorBackground:S_base1
URxvt*pointerColorForeground:S_base01

s_yellow  = sRGB24read "#b58900"
s_orange  = sRGB24read "#cb4b16"
s_red     = sRGB24read "#dc322f"
s_magenta = sRGB24read "#d33682"
s_violet  = sRGB24read "#6c71c4"
s_blue    = sRGB24read "#268bd2"
s_cyan    = sRGB24read "#2aa198"
s_green   = sRGB24read "#859900"

!! black dark/light
URxvt*color0:                S_base02
URxvt*color8:                S_base03

!! red dark/light
URxvt*color1:                S_red
URxvt*color9:                S_orange

!! green dark/light
URxvt*color2:                S_green
URxvt*color10:               S_base01

!! yellow dark/light
URxvt*color3:                S_yellow
URxvt*color11:               S_base00

!! blue dark/light
URxvt*color4:                S_blue
URxvt*color12:               S_base0

!! magenta dark/light
URxvt*color5:                S_magenta
URxvt*color13:               S_violet

!! cyan dark/light
URxvt*color6:                S_cyan
URxvt*color14:               S_base1

!! white dark/light
URxvt*color7:                S_base2
URxvt*color15:		S_base3

UXTerm*background:            S_base3
UXTerm*foreground:            S_base00
UXTerm*fadeColor:             S_base3
UXTerm*cursorColor:           S_base01
UXTerm*pointerColorBackground:S_base1
UXTerm*pointerColorForeground:S_base01
UXTerm*color0:                S_base02
UXTerm*color8:                S_base03
UXTerm*color1:                S_red
UXTerm*color9:                S_orange
UXTerm*color2:                S_green
UXTerm*color10:               S_base01
UXTerm*color3:                S_yellow
UXTerm*color11:               S_base00
UXTerm*color4:                S_blue
UXTerm*color12:               S_base0
UXTerm*color5:                S_magenta
UXTerm*color13:               S_violet
UXTerm*color6:                S_cyan
UXTerm*color14:               S_base1
UXTerm*color7:                S_base2
UXTerm*color15:		S_base3

URxvt.perl-ext:           default,matcher
URxvt.url-launcher:       /usr/bin/firefox-esr
URxvt.keysym.C-Delete:    perl:matcher:last
URxvt.keysym.M-Delete:    perl:matcher:list
URxvt.matcher.button:     1
! URxvt.matcher.pattern.1:  https:\/\/bugs.debian.org/(\\d+)
! URxvt.matcher.launcher.1: /home/corvus/bin/gertty --open $0

-}
