{-# LANGUAGE OverloadedStrings #-}

-- the battery monitor will cause a fatal error if upower is not installed

import System.Taffybar
import System.Taffybar.Hooks
import System.Taffybar.Information.Memory (parseMeminfo, MemoryInfo(..))
import System.Taffybar.SimpleConfig
import System.Taffybar.Widget.Battery
import System.Taffybar.Widget.CPUMonitor
import System.Taffybar.Widget.Decorators
import System.Taffybar.Widget.FreedesktopNotifications
import System.Taffybar.Widget.FSMonitor
import System.Taffybar.Widget.Generic.PollingGraph
import System.Taffybar.Widget.Layout
import System.Taffybar.Widget.SimpleClock
import System.Taffybar.Widget.SNITray
import System.Taffybar.Widget.Text.MemoryMonitor
import System.Taffybar.Widget.Text.NetworkMonitor
import System.Taffybar.Widget.Weather
import System.Taffybar.Widget.Windows
import System.Taffybar.Widget.Workspaces (workspacesNew, defaultWorkspacesConfig)

hiDPI = True

memCallback = do
  mi <- parseMeminfo
  return $ map ((/memoryTotal mi) . ($ mi)) [ memoryBuffer, memoryCache ]

main = do
  let cpuCfg = defaultGraphConfig { graphDataColors = [ (0, 1, 0, 1), (1, 0, 1, 0.5)]
                                  , graphLabel = Just "cpu"
                                  }
      memCfg = defaultGraphConfig { graphDataColors = [ (0, 1, 0, 1), (1, 0, 1, 0.5)]
                                  , graphLabel = Just "mem"
                                  }
      clock = textClockNew Nothing "<span fgcolor='orange'>%a %b %_d %H:%M:%S</span>" 1
      workspaces = workspacesNew defaultWorkspacesConfig
      tray = sniTrayNew -- sniTrayThatStartsWatcherEvenThoughThisIsABadWayToDoIt
      cpu = cpuMonitorNew cpuCfg 1.0 "cpu"
      mem = pollingGraphNew memCfg 4.0 memCallback
      wcfg = (defaultWeatherConfig "KLGA") { weatherTemplate = "KLGA: $tempC$℃@$humidity$%" }
      weather = weatherNew wcfg 10
      fs = fsMonitorNew 500.0 ["/home"]
      net_lan_wifi = networkMonitorNew "net" Nothing
      battery = batteryIconNew
      note = notifyAreaNew defaultNotificationConfig
      layout = layoutNew defaultLayoutConfig
      windowsW = windowsNew defaultWindowsConfig
      myConfig = defaultSimpleTaffyConfig {
                                          barHeight = if hiDPI then 60 else 30
                                        , startWidgets =
          workspaces : map (>>= buildContentsBox) [ layout, windowsW, note ]
                                        , endWidgets = map (>>= buildContentsBox) [
                                                         tray
                                                       , weather
                                                       , clock
                                                       , fs
                                                       , mem
                                                       , cpu
                                                       , net_lan_wifi
                                                       , battery
                                                       ]
                                        , barPosition = Top

                                        }
  dyreTaffybar $ withBatteryRefresh $ withLogServer $ withToggleServer $ toTaffyConfig myConfig
