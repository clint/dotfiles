#!/bin/bash
# YouTube Video Player
# Required: uzbl, youtube-dl, mplayer

# Uncomment one of these modes.
#FORMATS=()   # Play SD quality.
FORMATS=(22 18 6) # Play best HD quality available.
#FORMATS=(6 18 22) # Play worst HD quality available.

# Settings
MPLAYER_COMMAND=(mplayer -really-quiet -fs)
VALID_URL=0
exec &>/dev/null

# Plays the first format found.
case "$6" in
 (http://www.youtube.com/watch*)
  VALID_URL=1
  for f in "${FORMATS[@]}"; do
    URL=$(youtube-dl -f "$f" -g "$6")
    if [[ $URL ]] ; then
      "${MPLAYER_COMMAND[@]}" "$URL"
      exit
    fi
  done
  ;;
esac

if [[ $VALID_URL -eq 1 ]]; then
  "${MPLAYER_COMMAND[@]}" "$(youtube-dl -g "$6")"
fi
